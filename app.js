const express = require("express")
// const path = require('path')
const app = express()
//starting the server on port 4001

const userRouter = require('./routes/userRoutes')
// const viewRouter = require('./Routes/viewRoutes')

app.use(express.json())
app.use('/api/v1/users', userRouter)
// app.use('/', viewRouter)

// app.use(express.static(path.join(__dirname, 'views')))

module.exports = app
