const express = require('express')
const userController = require("./../controllers/userController")
// const authController = require('./../controllers/authController')
const router = express.Router();

// router.post('/signup', authController.signup)
// router.post('/login', authController.login)

router  
    .route('/')
    .get(userController.getAllUsers)
    .post(userController.createUser);
router
    .route('/:id')
    .get(userController.getUser)
    .patch(userController.updateUser)
    .delete(userController.deleteUser);

module.exports = router

// const express = require('express');


// const {
//     getAllUsers,
//     createUser,
//     getUserById,
//     updateUser,
//     deleteUser
// } = require ("../controllers/UserController");

// const router = express.Router();

// router.route("/").get(getAllUsers).post(createUser);
// router.route("/:id").get(getUserById).put(updateUser).delete(deleteUser);

// module.exports = router;