// const { JsonWebTokenError } = require('jsonwebtoken');
const User = require('../models/userModels');
// const userService=require('../Services/userServices');


// const jwt=require('jsonwebtoken')

// const signToken=(id)=>{
//     return jwt.sign({id},process.env.JWT_SECRET,{
//         expiresIn: process.env.Jwt_EXPIRES_IN,
//     })
// }
 exports.getAllUsers=async(req,res,next)=>{
    try{
        const users=await User.find()
        res.status(200).json({data:users,status:'success'})

    }catch(err){
        res.status(500).json({error:err.message});
    }
 };

 exports.createUser=async(req,res)=>{
    try{
        const user=await User.create(req.body);
        console.log(req.body.name)
        res.json({data: user,status:"success"});

    }catch(err){
        res.status(500).json({error:err.message});
    }
 };

 exports.getUser=async(req,res)=>{
    try{
        const users=await userService.getUserById(req.params.id);
        res.json({data:users,status:"success"});

    }catch(err){
        res.status(500).json({error:err.message});
    }
 };

 exports.updateUser=async(req,res)=>{
    try{
        const user=await User.findByIdAndUpdate(req.params.id,req.body);
        res.json({data:user,status:"success"});

    }catch(err){
        res.status(500).json({error:err.message});
    }
 };

 exports.deleteUser=async(req,res)=>{
    try{
        const user=await User.findByIdAndDelete(req.params.id);
        res.json({data:user,status:"success"});

    }catch(err){
        res.status(500).json({error:err.message});
    }
 };

//  exports.signup=async(req,res,next)=>{
//     try{
//         const newUser=await User.create(req.body)
//         const token=signToken(newUser._id)
//         res.status(201).json({
//             status:"success",
//             token,
//             data:{
//                 user:newUser
//             }
//         })
//     }
//     catch(err){
//         res.status(500).json({error:err.message});
//     }
//  }

