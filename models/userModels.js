const mongoose=require('mongoose')
const validator=require('validator')
// const bcrypt=require('bcryptjs')

const userSchema= new mongoose.Schema({
    name: {
        type:String,
        required:[true,'please tell us your name!'],

    },
    email:{
        type:String,
        required:[true,'please provide your email'],
        unique:true,
        lowercase:true,
        validate:[validator.isEmail,'please provide a valid email'],

    },
    photo:{
        type:String,
        default:'default.jpg',

    },
    role:{
        type:String,
        enum: ["user", "sme", "pharmacist", "admin"],
        default: "user"   
    },
    password:{
        type: String,
        required: [true, "Please provide a password!"],
        minlength: 8,
        select: false
    },
    passwordConfirm:{
        type:String,
        required:[true,'Please confirm your password'],
        validate:{
            //this only work on SAVE!!
            validator:function(el){
                return el ===this.password
            },
            message:'Password are not the same',
        },
    },
    active:{
        type:Boolean,
        default:true,
        select:false,
    },


})
userSchema.pre('save',async function(next){
    //only run this function if password was actually modified
    if(!this.isModifies('password')) return next()
    //Hash th epassword with cost of 12
    // this.password=await bcrypt.hash(this.password,12)
    // //delete passwordconfirm field
    // this.passwordConfirm=undefined
    // next()
})
const User=mongoose.model('user',userSchema)
module.exports=User
